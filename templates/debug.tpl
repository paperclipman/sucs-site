{if $compoutput != ""}
    <div class="clear"></div>
    <div class="error">
        <div class="errorhead">
            <h3>Unexpected Output From Component</h3>
        </div>
        <div class="errorcontent">
            {$compoutput}
        </div>
    </div>
{/if}

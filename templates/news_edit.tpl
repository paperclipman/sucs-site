<h2>Post a news item</h2>

<form class="admin" action="{$baseurl}{$path}" method="POST">
    <div class="row">
        <label for="edittitle">Title</label>
        <span class="textinput"><input type="text" name="edittitle" id="edittitle" value="{$news[0].title}"
                                       style="width: 100%;"/></span>
    </div>
    <div class="row">
        <label for="body">Body</label>
        <span class="textinput"><textarea name="body" id="body"
                                          style="width: 100%; height: 20em;">{$news[0].body|escape:'htmlall'}</textarea></span>
    </div>
    <div class="row">
        <label for="sticky">Stick to front page</label>
        <span class="textinput"><input type="checkbox" name="sticky"{if $news[0].sticky=='t'} checked="checked"{/if} /></span>
    </div>
    <div class="row">
                <span class="textinput">
                        <input type="submit" id="submit-1" name="action" value="Save"/>
                </span>
    </div>
    <input type="hidden" name="id" value="{$news[0].id}"/>

    <div class="clear"></div>
</form>


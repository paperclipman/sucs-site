{foreach from=$user_messages.error item=message}
    <div class="errorbar">
        <div>
            <div>
                <div>
                    {$message}
                </div>
            </div>
        </div>
    </div>
{/foreach}

{foreach from=$user_messages.warning item=message}
    <div class="errorbar">
        <div>
            <div>
                <div>
                    {$message}
                </div>
            </div>
        </div>
    </div>
{/foreach}

{foreach from=$user_messages.notice item=message}
    <div class="errorbar">
        <div>
            <div>
                <div>
                    {$message}
                </div>
            </div>
        </div>
    </div>
{/foreach}

{foreach from=$user_messages.info item=message}
    <div class="infobar">
        <div>
            <div>
                <div>
                    {$message}
                </div>
            </div>
        </div>
    </div>
{/foreach}

<div class="cbb">
    {if count($pasteList) > 0 }
        <ul id="lstSidebar">
            {foreach name=pasteList from=$pasteList item=pasteItem}
                <li><a href="{$baseurl}{$urifragment}{$pasteItem.id}"> {$pasteItem.name} - {$pasteItem.time_diff}
                        ago </a></li>
            {/foreach}
        </ul>
    {else}
        <p>None Available</p>
    {/if}
</div>

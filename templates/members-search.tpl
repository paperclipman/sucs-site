<div class="cbb">
    <h3>Search</h3>

    <form action="{$url}" method="post">
        <div>
            <p>Search for a member...</p>
            <input type="text" name="search"/>
            <input type="submit" name="submit" value="Search"/>
        </div>
    </form>
    <form action="{$url}" method="post">
        <div>
            <p>...Or select from the list</p>
            <select name="member">
                {foreach name=members from=$members item=member}
                    <option value="{$member.uid}">{$member.uid}</option>
                {/foreach}
            </select>
            <input name="submit" value="Display Details" type="submit"/>
        </div>
    </form>
</div>

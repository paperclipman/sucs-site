{include file="../static/fragments/Games.txt"}
<ul class="boxes">
    {foreach name=games from=$games item=game}
        <li>
            <div class="cbb"><a href="{$baseurl}/Games/{$game.name}">
                    <div>
                        <img src="{$baseurl}/pictures/games/{$game.name|escape:url}.jpg" alt="{$game.name}"/>
                        <br/>{$game.name}
                    </div>
                </a></div>
        </li>
    {/foreach}
</ul>

<?php

$dirlist = scandir("/var/projects/trac");

$projects = array();
foreach ($dirlist as $file) {
    if ($file[0] != ".") {
        $object = array();
        $object['filename'] = $file;
        $ini_array = array();
        $filepath = "/var/projects/trac/" . $file . "/conf/trac.ini";

        // read trac.ini for this project
        if (is_readable($filepath) && ($ini_data = file($filepath)) != FALSE) {

            $projectsection = FALSE;
            foreach ($ini_data as $ini_line) {
                if (trim($ini_line) == "[project]") {
                    $projectsection = TRUE;
                } else if ($projectsection && (1 == preg_match("/^\[.*\]$/", trim($ini_line)))) {
                    $projectsection = FALSE;
                } else if ($projectsection) {
                    list($key, $value) = preg_split("/=/", $ini_line);
                    $ini_array[trim($key)] = trim($value);
                }
            }

            // only list projects whose trac.ini sets public = true
            if (isset($ini_array['public']) && ($ini_array['public'] == "true")) {

                $object['name'] = $ini_array['name'];
                if ($object['name'] == "") $object['name'] = $file;

                $object['descr'] = $ini_array['descr'];

                $projects[$file] = $object;
            }
        }
    }
}

$smarty->assign("projects", $projects);


$output = $smarty->fetch("projects.tpl");
$smarty->assign("title", "Projects");
$smarty->assign("body", $output);

$sidebar = file_get_contents("../static/fragments/Projects-secondary.txt");
$smarty->assign("secondary", $sidebar);

?>

<?php
// guard against corrupt folder data
define("TOOBIG", 102400);

function load_folders()
{
    $f = fopen("/var/lib/mw/folders.bb", "r");

    $folders = array();
    while (!feof($f) && $fol = fread($f, 64)) {
        $data = unpack("Cstatus/A11name/A31topic/Cspare/Lfirst/Llast", $fol);
        cleanup($data['name']);
        cleanup($data['topic']);
        if ($data['status'] & 0x01 && !($data['status'] & 0x20)) $folders[$data['name']] = $data;
    }
    fclose($f);
    return $folders;
}

function cleanup(&$foo)
{
    $i = 0;
    $new = "";
    while ($i < strlen($foo)) {
        if (ord($foo[$i]) == 0) break;
        if (ctype_print($foo[$i]))
            $new .= $foo[$i];
        $i++;
    }
//	$foo = htmlentities($new);
    $foo = $new;
}


function load_index($folname, $getbody = FALSE)
{
    $index = array();
    $f = @fopen("/var/lib/mw/$folname.i", "r");
    $body = @fopen("/var/lib/mw/$folname.t", "r");
    if ($f === FALSE || $body === FALSE) return array();


    while (!feof($f) && $fol = fread($f, 128)) {
        $data = unpack("Lref/Ldate/A17from/A41to/A41subject/c1spare/Ldatafield/Lsize/Cstatus/a3spare/Lreplyto/C4spare", $fol);
        cleanup($data['subject']);
        cleanup($data['to']);
        cleanup($data['from']);
        // not deleted, has data, and want it, so load the body
        if (!($data['status'] & 0x02)) {
            if ($getbody && $data['size'] > 0 && $data['size'] < TOOBIG) {
                fseek($body, $data['datafield']);
                //$data['body'] = htmlentities(fread($body, $data['size']));
                $data['body'] = fread($body, $data['size']);
            }
            $index[$data['ref']] = $data;
        }
    }

    fclose($f);
    return $index;
}

// Count how many articles are replies to this one
function count_sub(&$haystack, $ref)
{
    $count = 0;
    foreach ($haystack as $k => $v) {
        if ($v['replyto'] == $ref) {
            $count++;
            $count += count_sub($haystack, $v['ref']);
        }
    }
    return $count;
}

function fetch_sub(&$all, $ref)
{
    $sub = array();
    foreach ($all as $k => $v) {
        if ($v['replyto'] == $ref) {
            $sub[$k] = $v;
            $sub = array_merge($sub, fetch_sub($all, $v['ref']));
        }
    }
    return $sub;
}

function post_article($folder, $user, $replyto)
{
    $folder = escapeshellarg($folder);
    $f = popen("/usr/bin/mw -f $folder $user $replyto", "w");
    $to = strip_tags($_POST['to']);
    $subject = strip_tags($_POST['subject']);
    $body = strip_tags($_POST['body']);

    $to = preg_replace('/[^[:alnum:][:punct:][:space:]]*/', '', trim($to));
    $subject = preg_replace('/[^[:alnum:][:punct:][:space:]]*/', '', trim($subject));
    $body = preg_replace("/\n\\.\n", "\n .\n/", $body);

    $deb = fopen("/tmp/mwdebug.txt", "w");
    fwrite($deb, "folder: '$folder'\n");
    fwrite($deb, "user: '$user'\n");
    fwrite($deb, "replyto: '$replyto'\n");
    fwrite($deb, "to: '$to'\n"); // Send to
    fwrite($deb, "sub: '$subject'\n"); // subject
    fwrite($deb, "body:\n$body\n.\n"); // end post

    if ($replyto == 0) {
        fwrite($f, "$to\n"); // Send to
        fwrite($f, "$subject\n"); // subject
        fwrite($f, "$body\n.\n"); // end post
    } else {
        fwrite($f, "$subject\n"); // subject
        fwrite($f, "$to\n"); // send to
        fwrite($f, "$body\n.\n"); // end post
    }
    $ret = pclose($f);
    fwrite($deb, "Exit = " . ($ret / 256) . "\n");
    fclose($deb);
}


if (!$session->loggedin) {
    $smarty->assign("title", "Milliways");
    $smarty->assign("body", "Sorry, you must be logged in for this feature");
    return;
}

$folders = load_folders();
$smarty->assignByRef("folders", $folders);
$smarty->assign("extra_styles", array("/css/forum/SUCS.css"));

unset($mode);
$last = end($pathlist);
if ($last == "new" || $last == "reply") {
    $mode = $last;
    array_pop($pathlist);
    $path = implode("/", $pathlist);
    /*	$smarty->assign("extra_scripts", array(
            '<script language="javascript" type="text/javascript" src="/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>',
            '<script language="javascript" type="text/javascript" src="/js/tiny_mce.js"></script>'
            ));
            */
}
if ($last == "post") {
    $mode = $last;
    array_pop($pathlist);
    $path = implode("/", $pathlist);
}
$smarty->assign("mode", $mode);

$shortpathlist = $pathlist;
array_pop($shortpathlist);
$shortpath = implode("/", $shortpathlist);
$smarty->assign("shortpath", $shortpath);

if (isset($pathlist[3])) {
    $name = $pathlist[3];
    if (isset($folders[$name])) {
        $smarty->assign("folder", $folders[$name]['name']);
        $all = load_index($name, TRUE);

        // One specific article was mentioned
        if (isset($pathlist[4])) {
            $artno = (int)$pathlist[4];
            if (isset($all[$artno])) {
                if ($mode == "post") {
                    post_article($name, $session->username, $artno);
                    $all = load_index($name, TRUE);
                }

                $smarty->assign("title", "Milliways");
                $smarty->assign_by_ref("message", $all[$artno]);

                $sub = fetch_sub($all, $all[$artno]['ref']);
                $smarty->assign_by_ref("articles", $sub);
                $smarty->assign("howmany", count($sub));
                $output = $smarty->fetch($base . "templates/mw-article.tpl");

            }
        } else {
            if ($mode == "post") {
                post_article($name, $session->username, 0);
                $all = load_index($name, TRUE);
            }
            // list all the articles in this folder
            $top = array();
            foreach ($all as $k => $v) {
                if ($v['replyto'] == 0) {
                    $v['suball'] = fetch_sub($all, $v['ref']);
                    $v['sub'] = count($v['suball']);
                    $l = end($v['suball']);
                    $v['lastpost'] = $l['date'];
                    $v['lastfrom'] = $l['from'];
                    $top[$k] = $v;
                }
            }
            $top = array_reverse($top);

            $smarty->assign("title", htmlentities("Milliways - Folder $name"));
            $smarty->assign_by_ref("articles", $top);
            $output = $smarty->fetch($base . "templates/mw-list.tpl");
        }
    } else {
        $output = "Error, no such folder $name";
    }
    $smarty->assign("body", $output);

} else {
    // List all the folders
    foreach ($folders as $k => $v) {
        $list = load_index($v['name']);
        $topics = 0;
        foreach ($list as $art) if ($art['replyto'] == 0) $topics++;
        $folders[$k]['topics'] = $topics;
        $lastone = end($list);
        $folders[$k]['lastpost'] = $lastone['date'];
    }
    $smarty->assign("title", "Milliways - Topic List");
    $output = $smarty->fetch($base . "templates/mw-folders.tpl");
    $smarty->assign("body", $output);
}

?>

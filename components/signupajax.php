<?php
require_once("../lib/validation.php");
// don't output the site template
$no_template = TRUE;
header("Content-type: text/plain");
if (isset($_GET['key'])) {
    switch ($_GET['key']) {
        case "sid":
            $sid = $_GET['value'];
            if (validSID($sid, false)) {
                echo "OK:" . lookupSID($_GET['value']);
            } else {
                echo "ERROR:" . $error;
            }
            break;
        case "postcode":
            $postcode = validPostcode($_GET['value']);
            if ($postcode != false) {
                lookup_postcode($postcode);
            }
            break;
        case "username":
            $username = $_GET['value'];
            if (validUsername($username)) {
                echo "OK";
            } else {
                echo $error;
            }
            break;
        case "realname":
            $realname = $_GET['value'];
            if (validName($realname, false)) {
                echo "OK";
            } else {
                echo $error;
            }
            break;
        case "socname":
            $socname = $_GET['value'];
            if (validName($socname, false)) {
                echo "OK";
            } else {
                echo $error;
            }
            break;


        case "address":
            $address = $_GET['value'];
            if (validAddress($address)) {
                echo "OK";
            } else {
                echo $error;
            }
            break;

        case "email":
            $email = $_GET['value'];
            if (validSignupEmail($email)) {
                echo "OK";
            } else {
                echo $error;
            }
            break;
        case "phone":
            $phone = $_GET['value'];
            if (validPhone($phone)) {
                echo "OK";
            } else {
                echo $error;
            }
            break;
    }
}
?>

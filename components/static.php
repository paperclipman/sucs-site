<?php
// Static page producer module

// A user must be in the html group in order to edit pages in this component
$permission = "html";

// Get the right filename...
$myfile = $base . "static" . $path . $language['file'] . ".txt";
$secondaryfile = $base . "static" . $path . $language['file'] . "-secondary.txt";
if (!file_exists($myfile)) {
    $myfile = $base . "static$path.txt";
    $secondaryfile = $base . "static$path-secondary.txt";
    // If the file doesn't exist...
    if (!file_exists($myfile)) {
        if (isset($session->groups[$permission]) && @$_REQUEST['action'] == "create") {
            // ...and we have permission and have been asked to, create it
            $body = "foo";
        } else {
            $redirect_path = $DB->GetOne("SELECT to_uri FROM redirect where from_uri=?", array("/" . $pathlist[1]));
            if (isset($_SERVER['HTTPS'])) $proto = "https://";
            else $proto = "http://";
            if ($redirect_path != "") {
                $variables = "";
                for ($i = 2; $i < count($pathlist); $i++) {
                    $variables .= "/" . $pathlist[$i];
                }
                $redirect_uri = $proto . $_SERVER['SERVER_NAME'] . $redirect_path . $variables;
                if ($_SERVER['QUERY_STRING'] != "") $redirect_uri .= "?" . $_SERVER['QUERY_STRING'];
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: " . $redirect_uri);
            } else {
                // ...serve a 404 error
                header("HTTP/1.1 404 Not Found");
                $body = @file_get_contents($base . "static/404.txt");
                $smarty->assign("pathlist", array("", "Error"));
                $title = "File not found";
                $smarty->assign("title", $title);
                $smarty->assign("body", $body);
                // Give those with permission the chance to create the page
                if (isset($session->groups[$permission])) $smarty->assign("creatable", TRUE);
            }
            return;
        }
    }
}

// If we've got edit permission...
if (isset($session->groups[$permission])) {
    switch (@$_REQUEST['action']) {
        case "Save":
            // ...save the file
            $savesuccess = @file_put_contents($myfile, $_REQUEST['body'], LOCK_EX);
            if (!$savesuccess) trigger_error("Write failed", E_USER_ERROR);
            $id = $DB->GetOne("select id from static where path=?", array($path));
            $record = array();
            $record['summary'] = $_REQUEST['summary'];
            $record['editor'] = $session->username;
            $record['path'] = $path;
            if ($id > 0) {
                $DB->AutoExecute("static", $record, 'UPDATE', "id = '" . $id . "'");
            } else {
                $DB->AutoExecute("static", $record, 'INSERT');
            }
            // probably need some error-handling here...
            break;
        case "Delete":
            $deleted = unlink($myfile);
            break;
    }
}

$title = end($pathlist);

if (file_exists($myfile)) {
    $body = file_get_contents($myfile);
    $modified = date("r", filectime($myfile));
    $modifiedby = $DB->GetOne("select editor from static where path=?", array($path));
}

if (file_exists($secondaryfile)) {
    $secondary = file_get_contents($secondaryfile);
    $smarty->assign("secondary", $secondary);
}

// include a widget for leaving feedback on this page if the user is logged in
if ($session->loggedin) {
//	include("../lib/page-feedback.php");
}

$smarty->assign("title", str_replace("_", " ", $title));

// Editing static pages - does the user have permission?
if (isset($session->groups[$permission])) {
// display Edit link on page
    $smarty->assign("editable", TRUE);

    switch (@$_REQUEST['action']) {
// load the editing template
        case "create":
            if (!file_exists($myfile)) file_put_contents($myfile, "Page under construction\n", LOCK_EX);
        case "edit":
            $smarty->assign("editcontent", $body);
            $record['summary'] = $DB->GetOne("select summary from static where path=?", array($path));
            $smarty->assign("record", $record);
            $smarty->assign("action", "edit");
            $modified = NULL;
            $body = $smarty->fetch('static_edit.tpl');
            break;
        case "delete-query":
            $body = $smarty->fetch('static_delete.tpl') . $body;
            break;
        case "Delete":
            if ($deleted) $body = "File deleted";
            break;
    }
}

$smarty->assign("body", $body);
$smarty->assign("modified", @$modified);
$smarty->assign("modifiedby", @$modifiedby);
?>

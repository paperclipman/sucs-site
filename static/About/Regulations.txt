<p>Below are links to the Rules and Regulations of the society:</p>

<h2>Data/Privacy Policy/Notice</h2>
<p>
    SUCS stores and processes data, details of which can be found in our 
    <a href="Data">Data Policy</a>.
</p>

<h2>Conditions of Membership</h2>
<p>
    All SUCS members must adhere to the society&#39;s 
    <a href="Conditions">Terms and Conditions</a>, which incorporate the 
    <a href="https://community.jisc.ac.uk/library/acceptable-use-policy">JANET 
    acceptable use policy</a> governing use of the SUCS network connection.
</p>

<h2>Room Rules</h2>
<p>
    There is an additional set of rules covering the use of the 
    <a href="Room">SUCS Computer Room</a>. Please take note of the 
    <a href="Room%20Rules">Rules of the Room</a> if you plan to make use of this
    facility.</p>  <h2>Constitution</h2> <p>The <a href="Constitution">SUCS 
    Constitution</a> states the aims and objectives of the society, along with 
    basic rules about how it is run.
</p>
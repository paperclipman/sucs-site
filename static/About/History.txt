<p>A complete history of SUCS has been compiled at <a href="http://twenty.sucs.org/">http://history.sucs.org</a> in celebration of SUCS's 20th anniversary.</p>
<h2>Past Execs</h2>
<p>This is the hall of fame for members of the elected executive.</p>
<p>The society was founded in the summer of 1988.</p>
<table border="0" class="border">
<tbody>
<tr>
<th>Year</th>
<th>President</th>
<th>Treasurer</th>
<th>Secretary</th>
<th>Publicity</th>
<th>Gaming</th>
</tr>
<tr>
<td>
<p><strong>2018</strong></p>
</td>
<td><strong><a href="/~vectre">vectre</a> (Alexander Moras)</strong></td>
<td><strong><a href="/~elbows">elbows</a> (Laurence Sebastian Bowes)</strong></td>
<td><strong><a href="/~xray_effect">xray_effect</a> (Ryan Williams)</strong></td>
<td><strong><em>None</em></strong></td>
<td><strong><a href="/~arcryalis">arcryalis</a> (Hywel Williams)</strong></td>
</tr>
<tr>
<td>
<p>2017</p>
</td>
<td><a href="/~andy">andy</a> (Andrew Vincent Pover)</td>
<td><a href="/~eggnog">eggnog</a> (George Henry Woolley)</td>
<td><a href="/~sgelfod98">sgelfod98</a> (Megan Warren-Davis)</td>
<td><em>None</em></td>
<td><a href="/~crox">crox</a> (Ciaran Crocker)</td>
</tr>
<tr>
<td>
<p>2016</p>
</td>
<td><a href="/~elbows">elbows</a> (Laurence Sebastian Bowes)</td>
<td><a href="/~unreturnable">unreturnable</a> (Isabel Jenkins)</td>
<td><a href="/~coatlus">coatlus</a> (Quetzal Mac Creamer Shorey)</td>
<td><a href="/~andy">andy</a> (Andrew Vincent Pover)</td>
<td><a href="/~gigosaurus">gigosaurus</a> (Christopher Lindsay Manners)</td>
</tr>
<tr>
<td>
<p>2015</p>
</td>
<td><a href="/~imranh">imranh</a> (Imran Hussain)</td>
<td><a href="/~stig">stig</a> (Priyan Gami)</td>
<td><a href="/~sockitwench">sockitwench</a> (Courtney Simone Vasile)</td>
<td><a href="/~andy">andy</a> (Andrew Vincent Pover)</td>
<td><a href="/~unreturnable">unreturnable</a> (Isabel Jenkins)</td>
</tr>
<tr>
<td>
<p>2014</p>
</td>
<td><a href="/~imranh">imranh</a> (Imran Hussain)</td>
<td><a href="/~stig">stig</a> (Priyan Gami)</td>
<td><a href="/~nenenxi">nenenxi</a> (Calum Mark Phillips)</td>
<td><a href="/~sambw">sambw</a> (Samuel Joseph Bust-Webber)</td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>
<p>2013</p>
</td>
<td><a href="/~rjames93">rjames93</a> (Rob James)</td>
<td><a href="/~grepwood">grepwood</a> (Michael Dec)</td>
<td><a href="/~shirelings89">shirelings89</a> (Lorah Woodward)</td>
<td><a href="/~melonxhead">melonxhead</a> (Jake Bailey)</td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>
<p>2012</p>
</td>
<td><a href="/~ed">ed</a> (Ed Smith)</td>
<td><a href="/~zephyr">zephyr</a> (Safi Dewshi)</td>
<td><a href="/~rjames93">rjames93</a> (Rob James)</td>
<td><a href="/~shirelings89">shirelings89</a> (Lorah Woodward)</td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>
2011
</td>
<td><a href="/~kais58">kais58</a> (Callum Massey)</td>
<td><a href="/~snowdrake">snowdrake</a> (Jess Hunt)</td>
<td><a href="/~ed">ed</a> (Ed Smith)</td>
<td><a href="/~hantudemon">hantudemon</a> (Joseph Bhart)</td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>
<p>2010</p>
</td>
<td><a href="/~tswsl1989">tswsl1989</a> (Tom Lake)</td>
<td><a href="/~gandalftheginger">gandalftheginger</a> (Ashley Morris)</td>
<td><a href="/~itsme">itsme</a> (Jon Gordon)</td>
<td><em>None</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>2009</td>
<td><a href="/~eclipse">eclipse</a> (Tim Clark)</td>
<td><a href="/~tswsl1989">tswsl1989</a> (Tom Lake)</td>
<td><a href="/~insacuri">insacuri</a> (Adam Mann)</td>
<td><a href="/~foshjedi2004">foshjedi2004</a> (Chris Melvin)</td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>2008</td>
<td><a href="/~grant">grant</a> (Grant James)</td>
<td><a href="/~worldinsideme">worldinsideme</a> (Nick Corlett)</td>
<td><a href="/~eclipse">eclipse</a> (Tim Clark)</td>
<td><a href="/~kyara">kyara</a> (Jocelyn Konrad-Lee)</td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>2007</td>
<td><a href="/~saya">saya</a> (Kat Meyrick)</td>
<td><a href="/~loki">loki</a> (Kristian Knevitt)</td>
<td><a href="/~elsmorian">Elsmorian</a> (Chris Elsmore)</td>
<td><a href="/~frosty">frosty</a> (James Frost)</td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>2006</td>
<td><a href="/~frosty">frosty</a> (James Frost)</td>
<td><a href="/~welshbyte">welshbyte</a> (Andrew Price)</td>
<td><a href="/~talyn256">talyn256</a> (Sean Handley)</td>
<td><a href="/~elsmorian">Elsmorian</a> (Chris Elsmore)</td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>2005</td>
<td><a href="/~welshbyte">welshbyte</a> (Andrew Price)</td>
<td><a href="/~stringfellow">stringfellow</a> (Stephen Pike)</td>
<td><a href="/~davea">davea</a> (Dave Arter)</td>
<td><a href="/~wedge">wedge</a> (William Blackstock)</td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>2004</td>
<td><a href="/~rollercow">rollercow</a> (Chris Jones)</td>
<td><a href="/~pwb">pwb</a> (Peter Berry)</td>
<td><a href="/~vortex">vortex</a> (Toby Talbot)</td>
<td><a href="/~zulu">zulu</a> (Fernando Loizides)</td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>2003</td>
<td><a href="/~dhilton">DHilton</a> (Dan Hilton)</td>
<td><a href="/~cj">cj</a> (Chris Jessop)</td>
<td><a href="/~ct">ct</a> (Chris Turner)</td>
<td><a href="/~aggie">aggie</a> (Jon Agland)</td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>2002</td>
<td><a href="/~smerf">Smerf</a> (Richard Hardy)</td>
<td><a href="/~theraven">TheRaven</a> (David Chisnall)</td>
<td><a href="/~doug">Doug</a> (Doug Gore)</td>
<td><em>None</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>2001</td>
<td><a href="/~sits">Sits</a> (Sitsofe Wheeler)</td>
<td><a href="/~theraven">TheRaven</a> (David Chisnall)</td>
<td><a href="/~sonic">Sonic</a> (David Chester)</td>
<td><a href="/~daveb">DaveB</a> (David Brooks)</td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>2000-2001</td>
<td><a href="/~sits">Sits</a> (Sitsofe Wheeler)</td>
<td><a href="/~paul">Paul</a> (Paul Adams)</td>
<td><a href="/~cmckenna">cmckenna</a> (Chris McKenna)</td>
<td><a href="/~jo">Jo</a> </td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>1999-2000</td>
<td><a href="/~firefury">FireFury</a> (Steve Hill)</td>
<td><a href="/~rhys">Rhys</a> (Rhys Jones) &amp;<br /><a href="../~">Paul</a> (Paul Adams)</td>
<td><a href="/~fry">Fry</a> (Chris Fry)</td>
<td><em>N/A</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>1998-1999</td>
<td><a href="/~dez">Dez</a> (Denis Walker)</td>
<td><a href="/~rhys">Rhys</a> (Rhys Jones)</td>
<td><a href="/~firefury">FireFury</a> (Steve Hill)</td>
<td><em>N/A</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>1997-1998</td>
<td><a href="/~dez">Dez</a> (Denis Walker)</td>
<td><a href="/~rhys">Rhys</a> (Rhys Jones)</td>
<td><em>None</em></td>
<td><em>N/A</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>1996-1997</td>
<td>Ramps (Brian Coplin)</td>
<td><a href="/~zute/">Zute</a> (Matthew Suter)<br /></td>
<td><em>None</em></td>
<td><em>N/A</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>1995-1996</td>
<td>Eldritch (Jon Lewis)</td>
<td><a href="/~xadraek">Xadraek</a> (Lee Stephens)</td>
<td><a href="/~rohan">Rohan</a> (Steven Whitehouse)</td>
<td><em>N/A</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>1994-1995</td>
<td><a href="/~milamber">Milamber</a> (Andrew Hogg)</td>
<td><a href="/~xadraek">Xadraek</a> (Lee Stephens)</td>
<td><a href="/~rohan">Rohan</a> (Steven Whitehouse)</td>
<td><em>N/A</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>1993-1994</td>
<td><a href="/~nympho">Nympho</a> (Mike Evans)</td>
<td><a href="/~milamber">Milamber</a> (Andrew Hogg)</td>
<td><a href="/~rohan">Rohan</a> (Steven Whitehouse)</td>
<td><em>N/A</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>1992-1993</td>
<td><a href="/~mocelet">Mocelet</a> (Richard Bytheway)</td>
<td>&nbsp;</td>
<td><a href="/~gerbil">Gerbil</a> (Debbie Gwynne)</td>
<td><em>N/A</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>1991-1992</td>
<td><a href="/~arthur">Arthur</a> (Justin Mitchell)</td>
<td>Michele Poole</td>
<td>Adrian Bateman</td>
<td><em>N/A</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>1990-1991</td>
<td>Pete Barber</td>
<td><a href="/~alexw">alexw</a> (Alex Williams)</td>
<td><a href="/~cariad">Cariad</a> (Paul Rees)</td>
<td><em>N/A</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>1989-1990</td>
<td>Andy Parkman</td>
<td><a href="/~alexw">alexw</a> (Alex Williams)</td>
<td><a href="/~caederus">caederus</a> (Robin O'Leary)</td>
<td><em>N/A</em></td>
<td><em>N/A</em></td>
</tr>
<tr>
<td>1988-1989</td>
<td>Andy Parkman</td>
<td><a href="/~alexw">alexw</a> (Alex Williams)</td>
<td><a href="/~caederus">caederus</a> (Robin O'Leary)</td>
<td><em>N/A</em></td>
<td><em>N/A</em></td>
</tr>
</tbody>
</table>
<p>In 2001, the elections were moved so that the committee now stays in position for one calendar year.</p>
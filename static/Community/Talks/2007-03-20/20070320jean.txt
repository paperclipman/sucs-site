<div class="box">
	<div class="boxhead"><h2>Jean van Mourik (jean) - &quot;3D Programming in DarkBASIC&quot;</h2></div>
	<div class="boxcontent">
	<p>Jean van Mourik presents some fun and interesting applications he&#39;s written with DarkBASIC, from a 3D forest with 3D sound sources, to a graphical demonstration of complex numbers, to a 3D particle physics demonstration.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-03-20/jean.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-03-20/welshbyte.flv" image="/videos/talks/2007-03-20/welshbyte_preview.png" id="player" displayheight="240">
<param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-03-20/jean.flv" />
<param name="image" value="/videos/talks/2007-03-20/jean_preview.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-03-20/jean_preview.png" />
</object></div>

<p><strong>Length: </strong>10m 40s</p><p><strong>Video: </strong><a href="/videos/talks/2007-03-20/2007-03-20-jean.ogg" title="784x576 Ogg Theora - 144.7MB">784x576</a> (Ogg Theora, 144.7MB)<br /><strong>Example programs used in talk:</strong> <a href="/videos/talks/2007-03-20/2007-03-20-jean-examples.zip" title="Example programs">2007-03-20-jean-examples.zip</a> (Windows only, 4MB)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>

<div class="box">
	<div class="boxhead"><h2>Sean Handley (talyn256) - Why WEP Is Broken</h2></div>
	<div class="boxcontent">
	<p>WEP is used for security in countless wireless devices across the globe. But, it has been proven to be completely and utterly broken. Sean tells us why.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-05-15/2007-05-15-talyn256.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-05-15/2007-05-15-talyn256.flv" image="/videos/talks/2007-05-15/2007-05-15-talyn256.png" id="player" displayheight="240"><param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-05-15/2007-05-15-talyn256.flv" />
<param name="image" value="/videos/talks/2007-05-15/2007-05-15-talyn256.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-05-15/2007-05-15-talyn256.png" />
</object></div>

<p><strong>Length: </strong>9m 4s</p>
<p><strong>Video:</strong><a href="/videos/talks/2007-05-15/2007-05-15-talyn256.mov" title="784x576 H.264 .mov - 62.8MB">784x576</a>(H.264 .mov, 62.8MB98.3MB)</p>
<p><strong>Slides:</strong>Coming Soon (PDF, -)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>

var validation = {
    "studentid": false,
    "username": false,
    "realname": false,
    "address": false,
    "contact": false,
    "email": false,
    "phone": false
};
var submitted = false;
function validate() {
    var valid = true;
    var field;
    for (field in req) {
        if (!validation[req[field]]) {
            valid = false;
            break;
        }
    }
    if ((valid || ($('input#override:checked').size() == 1)) && !submitted && ($('input#tnc:checked').size() == 1)) {
        $('input#submit').removeAttr("disabled");
        return true;
    }
    else {
        $('input#submit').attr("disabled", "disabled");
        return false;
    }


}
function processPostcode() {
    // lookup postcode
    $.getJSON("signup/ajax", {key: "postcode", value: $('input#postcode').val()}, function (j) {
        //populate dropdown
        var options = '';
        if (j.addresses.length > 1) {
            // make dropdown visible
            $('div#addseldiv').removeAttr("style");
            for (var i = 0; i < (j.addresses.length); i++) {
                options += '<option>';
                if (j.addresses[i].flat !== null) {
                    options += j.addresses[i].flat + "\n";
                }
                if (j.addresses[i].house !== null) {
                    options += j.addresses[i].house + "\n";
                }
                if (j.addresses[i].road !== null) {
                    options += j.addresses[i].road + "\n";
                }
                if (j.addresses[i].city !== null) {
                    options += j.addresses[i].city + "\n";
                }
                options += '</option>';
            }
            $("select#addsel").html(options);
            $('select#addsel option:first').attr('selected', 'selected');
        }
        if (j.addresses.length == 1) {
            $('div#addseldiv').attr("style", "display:none");
            $("textarea#address");
            $('div#addressmessage').attr("style", "color:green; float:right; clear:right;");
            $('div#addressmessage').html("OK");
            validation.address = true;
            validate();
        }
    });
}

function lookupSID(setname) {
    $.get("/signup/ajax", {key: "sid", value: $('input#studentid').val()}, function (j) {
        arr = j.split(":");
        key = arr.shift();
        val = arr.join(":");
        if (key == "OK") {
            if (setname === true)
                $("input#realname").val(val);
            $('div#studentidmessage').attr("style", "color:green; float:right; clear:right;");
            $('div#studentidmessage').html(key);
            validation.studentid = true;
            processName("realname", "realname");

        }
        else {
            $('div#studentidmessage').attr("style", "color:red; float:right; clear:right;");
            $('div#studentidmessage').html(val);
            validation.email = false;
            validate();
        }
    }, 'text');
}
function processSID() {
    lookupSID(true);
}

function processUsername() {
    $.get("/signup/ajax", {key: "username", value: $('input#username').val()}, function (j) {
        if (j != "OK") {
            $('div#usernamemessage').attr("style", "color:red; float:right; clear:right;");
            validation.username = false;
        }
        else {
            $('div#usernamemessage').attr("style", "color:green; float:right; clear:right;");
            validation.username = true;
        }
        $('div#usernamemessage').html(j);
        validate();
    }, 'text');
}

function processName(type, input) {
    $.get("/signup/ajax", {key: type, value: $('input#' + input).val()}, function (j) {
        if (j != "OK") {
            $('div#' + input + 'message').attr("style", "color:red; float:right; clear:right;");
            validation[input] = false;
        }
        else {
            $('div#' + input + 'message').attr("style", "color:green; float:right; clear:right;");
            validation[input] = true;
        }
        $('div#' + input + 'message').html(j);
        validate();
    }, 'text');
}
function processContact() {
    processName('realname', 'contact');
}

function processAddress() {
    $.get("/signup/ajax", {key: "address", value: $('textarea#address').val()}, function (j) {
        if (j != "OK") {
            $('div#addressmessage').attr("style", "color:red; float:right; clear:right;");
            validation.address = false;
        }
        else {
            $('div#addressmessage').attr("style", "color:green; float:right; clear:right;");
            validation.address = true;
        }
        $('div#addressmessage').html(j);
        validate();
    }, 'text');
}

function processEmail() {
    $.get("/signup/ajax", {key: "email", value: $('input#email').val()}, function (j) {
        if (j != "OK") {
            $('div#emailmessage').attr("style", "color:red; float:right; clear:right;");
            validation.email = false;
        }
        else {
            $('div#emailmessage').attr("style", "color:green; float:right; clear:right;");
            validation.email = true;
        }
        $('div#emailmessage').html(j);
        validate();
    }, 'text');
}

function processPhone() {
    $.get("/signup/ajax", {key: "phone", value: $('input#phone').val()}, function (j) {
        if (j != "OK") {
            $('div#phonemessage').attr("style", "color:red; float:right; clear:right;");
            validation.phone = false;
        }
        else {
            $('div#phonemessage').attr("style", "color:green; float:right; clear:right;");
            validation.phone = true;
        }
        $('div#phonemessage').html(j);
        validate();
    }, 'text');
}

$(function () {
    if ($('input#studentid').size() == 1) {
        usertype = 1;
    }
    else if ($('input#contact').size() == 1) {
        usertype = 2;
    }
    else {
        usertype = 5;
    }
    req = new Array("username", "realname", "email", "phone");
    switch (usertype) {
        case "1":
            req.push("studentid", "address");
            break;
        case "2":
            req.push("contact");
            break;
        case "5":
            req.push("address");
    }
    //usertype=$('input#usertype').val();
    $("document").ready(function () {
        // makes script sutff appear
        $('div#postcodediv').removeAttr("style");
        $('input#submit').attr("disabled", "disabled");
        // if the fields are not empty validate them
        if ($('input#username').val() !== "") processUsername();
        if ($('input#email').val() !== "") processEmail();
        if ($('input#phone').val() !== "") processPhone();
        if (usertype !== 2) {
            if ($('input#postcode').val() !== "") processPostcode();
            if ($('textarea#address').val() !== "") processAddress();
            if ($('input#realname').val() !== "") processName("realname", "realname");
        }
        else {
            if ($('input#contact').val() !== "") processContact();
            if ($('input#realname').val() !== "") processName("socname", "realname");
        }
        if ((usertype == 1) && ($('input#studentid').val() !== "")) {
            if ($('input#realname').val() === "") lookupSID(true);
            else lookupSID(false);
        }

        validate();
    });
    // dont do address stuff for societies
    if (usertype != 2) {
        // process postcode when the box changes
        $("input#postcode").change(processPostcode);

        //populate the address box when an address is selected
        $("select#addsel").change(function () {

            // everything is shit and will strip out the line breaks
            // so we have to do weird shit with innerHTML

            // store the entire options as e
            var e = document.getElementById("addsel");
            // store the selected address's innerHTML as strUser (because stackpver flow copy and paste)
            var strUser = e.options[e.selectedIndex].innerHTML;
            // set the text area to the strUser var
            $("textarea#address").text(strUser);
            $('div#addressmessage').attr("style", "color:green; float:right; clear:right;");
            $('div#addressmessage').html("OK");
            validation.address = true;
        });
    }
    //else deal with the contact field
    else {
        $("input#contact").change(processContact);

    }
    //if is a student
    if (usertype == 1) {
        //lookup the real name from the studentid
        $("input#studentid").change(processSID);
    }
    //validate username
    $("input#username").change(processUsername);
    //validate personal and society names differntly
    if (usertype != 2) {
        persoc = 'realname';
    }
    else {
        persoc = 'socname';
    }
    //validate real name
    $("input#realname").change(function () {
        processName(persoc, 'realname');
    });

    //validate email address
    $("input#email").change(processEmail);
    //validate phone number
    $("input#phone").change(processPhone);
    $("textarea#address").change(processAddress);
    $("input#override").change(validate);
    $("input#tnc").change(validate);
    // Disable the submit button once clicked
    $("form#mainform").submit(function () {
        var valid = validate();
        if (valid) {
            // mark form as submitted
            submitted = true;
            // disable the submit button
            $('input#submit').attr("disabled", "disabled");
        }
        return valid;
    });
});

